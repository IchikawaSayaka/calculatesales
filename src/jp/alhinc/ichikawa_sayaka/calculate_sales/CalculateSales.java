package jp.alhinc.ichikawa_sayaka.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) {
		//System.out.println("引数の一列目のファイルをひらく => " + args[0]);
		//C:\Users\ichikawa.sayaka\eclipse\workspace\CalculateSales\売り上げ集計課題
		
		//Mapは全体通して使うので最初に変数名を設定する
		//定義ファイル
		HashMap<String, String> nameMap = new HashMap<String,String>();
		//売り上げファイルリスト
		HashMap<String, Long> salesMap = new HashMap<String,Long>();
		
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	
		//支店定義ファイルの読み込みをメソッドわけで行う
		String path = args[0];
		if(inputFile(path, "branch.lst", "支店", salesMap, nameMap, "^[0-9]{3}$") == false) {
			return;
		}
		
		//ディレクトリのファイルをすべて読み込む
		File file = new File(path);
		File files[]  = file.listFiles();
//		System.out.println("ディレクトリ内のファイルの数" + f.length);
		
		//売り上げファイル3つをあわせたリストをつくる
		//空のファイルを用意する
		BufferedReader br2 = null;
		ArrayList<File> rcdFiles = new ArrayList<File>();
		ArrayList<Integer> rcdFilesName = new ArrayList<>();
		try {
			for(int i = 0; i < files.length; i++){
				if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd")) {
					rcdFiles.add(files[i]);	
					String[] splitedRcdFiles = files[i].getName().split("\\.");
//					System.out.println(splitedRcdFiles[0]);
					int fileName = Integer.parseInt(splitedRcdFiles[0]);
					rcdFilesName.add(fileName);
				}			
			}
			Collections.sort(rcdFilesName);
			for(int i = 0; i < rcdFilesName.size() - 1; i++){
				if(rcdFilesName.get(i + 1) - rcdFilesName.get(i) != 1){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}			
				
			for(int i = 0; i < rcdFiles.size(); i++){
				//for文の中に売り上げファイル1個ずつの箱をつくる
				ArrayList<String> salesList = new ArrayList<>(); 
				br2 = new BufferedReader(new FileReader(rcdFiles.get(i)));
				//1行ずつ読み込む???
				String line2;
				while((line2 = br2.readLine()) != null) {		
					//1行ずつ売り上げリストに書き込む
					salesList.add(line2);								
				}
					
				if(salesList.size() != 2){
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}
				if(!nameMap.containsKey(salesList.get(0))) {
					System.out.println(files[i].getName() + "の支店コードが不正です");
					return;
				}
			
				//Sting型をlong型に変換し、事前につくったsalesListに本日の売り上げをたし算する　//??
				long aftersale = salesMap.get(salesList.get(0)) + Long.parseLong(salesList.get(1)); 						
							
				if(String.valueOf(aftersale).length() > 10){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				salesMap.put(salesList.get(0),aftersale);
			}
			
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally{
			if(br2 != null) {
				try {
					br2.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
		
		//salesMapを金額順に並べなおす	
		List<Map.Entry<String,Long>> entries = new ArrayList<Map.Entry<String,Long>>(salesMap.entrySet());
        Collections.sort(entries, new Comparator<Map.Entry<String,Long>>() {
            @Override
            public int compare(Entry<String,Long> entry1, Entry<String,Long> entry2) {
                return ((Long)entry2.getValue()).compareTo((Long)entry1.getValue());
            }
        });
	
		//売上集計ファイルの作成をメソッドわけで行う
	    //args[0]の中にbanch.outという名前のファイルをつくる
		if(outputFile(path, "branch.out", entries, nameMap) == false){
			return;
		}
	}

	//定義ファイル入力のメソッドわけ
	//メソッド内では汎用性のある名前をつける
	public static boolean inputFile(String path, String fileName, String dif, HashMap<String, Long> salesMap, HashMap<String, String> nameMap, String condition) {	
		BufferedReader br = null;
		try {
			File difFile = new File(path, fileName);
			//File(parent,child) parentパス名文字列はディレクトリを示し、childパス名文字列はディレクトリまたはファイルを示す。
		
			if (!difFile.exists()) {
				System.out.println( dif + "定義ファイルが存在しません" );
				return false;
			}
			
			br = new BufferedReader(new FileReader(difFile));
		
			String line1;
			while((line1 = br.readLine()) != null) {	
				//1行の文字列を","で区切る。
				String[] items = line1.split(",");	
				
				//正規表現は""で囲む,ほかには"\\d{3}"。3桁の数字か確認する
				if((items.length != 2) || (!items[0].matches(condition))){
					System.out.println(dif + "定義ファイルのフォーマットが不正です");	
					return false;
				}

				
				//支店コードと支店名を維持する。変数を{}の外に指定すれば保持できる
				nameMap.put(items[0], items[1]); 
				//売り上げファイル集計用のMapを用意しておく
				salesMap.put(items[0],0L);					
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					return false;
				}
			}
		}
		return true;
	}
	
	//売上集計ファイルのメソッドわけ
	public static boolean outputFile(String path, String fileName, List<Map.Entry<String,Long>> entries, HashMap<String, String> nameMap) {	
		File sumfile = new File(path, fileName);		
		BufferedWriter bw = null;
		try{
			bw = new BufferedWriter(new FileWriter(sumfile));

			for (Entry<String,Long> entry : entries) {
				bw.write(entry.getKey() + "," + nameMap.get(entry.getKey()) + "," + entry.getValue()); 
				bw.newLine();
			}
			
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
		return true;	
	}
}
